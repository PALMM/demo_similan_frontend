import { useState } from "react";
import "./Grade.css";
export default function Grade() {

    const [name, setName] = useState('')
    const [studentID, setStudentID] = useState('')
    const [score, setScore] = useState()
    const [grade, setGrade] = useState('')

    function calGrade(){
        if(score > 90 && score <= 100){
            setGrade('A')
        }else if(score > 80 && score <= 89.99){
            setGrade('B')
        }else if(score > 70 && score <= 79.99){
            setGrade('C')
        }else if(score > 60 && score <= 69.99){
            setGrade('D')
        }else if(score > 0 && score <= 59.99){
            setGrade('F')
        }
    }
  return (
    <div className="contrainer">
      <div className="grade-contrainer">
        <h1>คำนวนเกรด</h1>
        <p>ชื่อ - สกุล</p>
        <input type="text" value={name} onChange={e => setName(e.target.value)}></input>
        <p>รหัสนักศึกษา</p>
        <input type="text" value={studentID} onChange={e => setStudentID(e.target.value)}></input>
        <p>ระบุคะเเนนของคุณ</p>
        <input type="text" value={score} onChange={e => setScore(e.target.value)}></input>
        <button onClick={() => calGrade()}>คำนวณ</button>
        <p>ชื่อของคุณคือ : {name}</p>
        <p>รหัสนักศึกษาของคุณคือ : {studentID}</p>
        <p>คะเเนนของคุณคือ : {score}</p>
        <p>เกรดของคุณคือ : {grade}</p>
      </div>
    </div>
  );
}
