import { Link } from "react-router-dom";
import "./Navbar.css";
export default function Navbar() {
  return (
    <nav>
      <div className="similan">
        <Link to="/">SIMILAN WEB EXAM</Link>
      </div>
      <Link to="/">Home</Link>
      <Link to="/grade">Grade</Link>
      <Link to="/calculator">Calculator</Link>
      <Link to="/crud">CRUD</Link>
    </nav>
  );
}
