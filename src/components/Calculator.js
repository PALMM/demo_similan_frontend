import { useState } from "react";
import "./Calculator.css";
export default function Calculator() {
  const [num1, setNum1] = useState();
  const [num2, setNum2] = useState();
  const [menu, setMenu] = useState("");
  const [result, setResult] = useState();

  function calResult() {
    if (menu === "1") {
      setResult(Number(num1) + Number(num2));
    } else if (menu === "2") {
      setResult(Number(num1) - Number(num2));
    } else if (menu === "3") {
      setResult(Number(num1) * Number(num2));
    } else if (menu === "4") {
      setResult(Number(num1) / Number(num2));
    } else if (menu === "5") {
      setResult(Number(num1) - Number(num2));
    }
  }

  return (
    <div className="contrainer">
      <div className="row">
        <p>Number 1 : </p>
        <input
          type="text"
          value={num1}
          onChange={(e) => setNum1(e.target.value)}
        />
      </div>
      <div className="row">
        <p>Number 2 : </p>
        <input
          type="text"
          value={num2}
          onChange={(e) => setNum2(e.target.value)}
        />
      </div>
      <div className="column">
        <p>Calculator Menu : </p>
        <p>1. +</p>
        <p>2. -</p>
        <p>3. *</p>
        <p>4. /</p>
        <p>5. %</p>
      </div>

      <div className="row">
        <p>Choose Menu : </p>
        <input
          type="text"
          value={menu}
          onChange={(e) => setMenu(e.target.value)}
        />
      </div>
        <button onClick={() => calResult()} className="btn-result">Calculate</button>
        <p>Result : {result}</p>
    </div>
  );
}
