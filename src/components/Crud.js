//npm run dev
import { Form, Input, Row, Col, Button } from "antd";
import "./Crud.css";
import { useState, useEffect } from "react";
import axios from "axios";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

export default function Crud() {
  const initialFormData = {
    id: "",
    name: "",
    address: "",
    postcode: "",
    phone: "",
    fax: "",
    email: "",
  };

  const [isFormValid, setIsFormValid] = useState(false);
  const [formData, setFormData] = useState(initialFormData);
  const [rows, setRows] = useState([]);
  const [select, setSelect] = useState();

  function getData() {
    axios.get("http://localhost:8000/read").then((response) => {
      console.log(response.data);
      const data = response.data.map((item) => ({
        id: item.cust_id,
        name: item.cust_name,
        address: item.cust_address,
        postcode: item.cust_postcode,
        phone: item.cust_phone,
        fax: item.cust_fax,
        email: item.cust_email,
      }));
      setRows(data);
    });
  }

  function saveData() {
    axios
      .post("http://localhost:8000/create", formData)
      .then((response) => {
        console.log(response.data);
        setFormData(initialFormData);
      })
      .catch((error) => {
        console.error("Error creating user:", error);
      });
  }

  // function editData(s) {
  //   console.log(s);
  // }

  function editData() {
    axios
      .put(`http://localhost:8000/update/${select}`, formData)
      .then((response) => {
        console.log(response.data);
        setFormData(initialFormData);
      })
      .catch((error) => {
        console.error("Error updating user:", error);
      });
  }

  function deleteData(x) {
    axios
      .delete(`http://localhost:8000/delete/${x}`, formData)
      .then((response) => {
        console.log(response.data);
        setFormData(initialFormData);
      })
      .catch((error) => {
        console.error("Error creating user:", error);
      });
  }

  function handleChange(name, value) {
    setFormData({ ...formData, [name]: value });
  }

  function validateForm() {
    const { id, name, address, postcode, phone, fax, email } = formData;
    setIsFormValid(id && name && address && postcode && phone && fax && email);
  }

  function selectRow(x) {
    setSelect(x)
    const selectedRow = rows.find((row) => row.id === x);
    setFormData(selectedRow);
  }

  useEffect(() => {
    getData();
    validateForm();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formData]);

  return (
    <div>
      <div className="form">
        <Form layout="vertical" onValuesChange={validateForm}>
          <Row gutter={160}>
            <Col span={10}>
              <Form.Item label="รหัสลูกค้า" required>
                <Input
                  className="input-space"
                  value={formData.id}
                  onChange={(e) => handleChange("id", e.target.value)}
                  name="id"
                />
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item label="ชื่อลูกค้า" required>
                <Input
                  className="input-space"
                  value={formData.name}
                  onChange={(e) => handleChange("name", e.target.value)}
                  name="name"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={20}>
              <Form.Item label="ที่อยู่ลูกค้า" required>
                <Input.TextArea
                  className="input-area"
                  value={formData.address}
                  onChange={(e) => handleChange("address", e.target.value)}
                  name="address"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={160}>
            <Col span={10}>
              <Form.Item label="รหัสไปรษณีย์" required>
                <Input
                  className="input-space"
                  value={formData.postcode}
                  onChange={(e) => handleChange("postcode", e.target.value)}
                  name="postcode"
                />
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item label="เบอร์โทรศัพท์" required>
                <Input
                  className="input-space"
                  value={formData.phone}
                  onChange={(e) => handleChange("phone", e.target.value)}
                  name="phone"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={160}>
            <Col span={10}>
              <Form.Item label="เบอร์แฟกซ์" required>
                <Input
                  className="input-space"
                  value={formData.fax}
                  onChange={(e) => handleChange("fax", e.target.value)}
                  name="fax"
                />
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item label="อีเมล" required>
                <Input
                  className="input-space"
                  value={formData.email}
                  onChange={(e) => handleChange("email", e.target.value)}
                  name="email"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={7}>
              <Button
                className="btn-save"
                onClick={() => saveData()}
                disabled={!isFormValid}
              >
                บันทึก
              </Button>
            </Col>
            <Col span={7}>
              <Button className="btn-edit" onClick={() => editData(select)}>
                เเก้ไข
              </Button>
            </Col>
            <Col span={7}>
              <Button className="btn-delete" onClick={() => deleteData(select)}>
                ลบ
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
      <div className="table">
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">รหัสลูกค้า</TableCell>
                <TableCell align="center">ชื่อลูกค้า</TableCell>
                <TableCell align="center">ที่อยู่ลูกค้า</TableCell>
                <TableCell align="center">รหัสไปรษณีย์</TableCell>
                <TableCell align="center">เบอร์โทรศัพท์</TableCell>
                <TableCell align="center">เบอร์แฟกซ์</TableCell>
                <TableCell align="center">อีเมล</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow
                  onClick={() => selectRow(row.id)}
                  key={row.id}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="center">{row.name}</TableCell>
                  <TableCell align="center">{row.address}</TableCell>
                  <TableCell align="center">{row.postcode}</TableCell>
                  <TableCell align="center">{row.phone}</TableCell>
                  <TableCell align="center">{row.fax}</TableCell>
                  <TableCell align="center">{row.email}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}
