import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./components/Home";
import Grade from "./components/Grade";
import Calculator from "./components/Calculator";
import Navbar from "./components/Navbar";
import Crud from "./components/Crud";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/grade" element={<Grade />}></Route>
          <Route path="/calculator" element={<Calculator />}></Route>
          <Route path="/crud" element={<Crud />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
